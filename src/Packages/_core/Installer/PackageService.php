<?php
/**
 * Created by PhpStorm.
 * User: lukys
 * Date: 2.10.16
 * Time: 23:25
 */

namespace CPTeam\Packages;

/**
 * Class TestPackage
 *
 * @package CPTeam\Packages\TestPackage
 *
 * @property $name;
 */
class PackageService
{
	
	/**
	 * @var array
	 */
	private $package = [];
	
	/**
	 * @return array
	 */
	public function getPackages()
	{
		return $this->package;
	}
	
	/**
	 * @param $name
	 *
	 * @return mixed
	 */
	public function getPackage($name)
	{
		if (isset($this->package[$name])) {
			return $this->package[$name];
		}
		
		throw new UndefinedPackageException("Package $name not found.");
	}
	
	/**
	 * @param $package
	 */
	public function addPackage(\CPTeam\Packages\Package $package)
	{
		$package->setPackageService($this);
		
		$this->package[] = $package;
	}
}

class UndefinedPackageException extends PackageException
{
	
}
