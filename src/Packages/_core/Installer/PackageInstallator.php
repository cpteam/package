<?php
/**
 * Created by PhpStorm.
 * User: lukys
 * Date: 2.10.16
 * Time: 23:25
 */

namespace CPTeam\Packages\TestPackage;

use Nette\DI\ContainerBuilder;

/**
 * Class PackageInstallator
 *
 * @package CPTeam\Packages\TestPackage
 */
abstract class PackageInstallator
{
	/**
	 * @var string
	 */
	public $name;
	
	/**
	 * @var
	 */
	protected $builder;
	
	/**
	 * PackageInstallator constructor.
	 *
	 * @param ContainerBuilder $builder
	 * @param $name
	 */
	public function __construct(ContainerBuilder $builder, $name)
	{
		$this->builder = $builder;
		$this->name = $name;
	}
	
	/**
	 * @param $msg
	 *
	 * @return string
	 */
	public function prefix($msg)
	{
		return "package." . $this->name . "." . $msg;
	}
	
	/**
	 * @param array $config
	 */
	abstract public function install(array $config);
	
}
