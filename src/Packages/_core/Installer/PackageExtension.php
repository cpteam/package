<?php

namespace CPTeam\Packages\Installer;

use CPTeam\Nette\Router\RouterFactory;
use CPTeam\Packages\Admin\Presenters\TBasePresenter;
use CPTeam\Packages\Package;
use CPTeam\Packages\PackageException;
use CPTeam\Packages\PackageService;
use CPTeam\Packages\TestPackage\PackageInstallator;
use Nette\Application\IPresenterFactory;
use Nette\Application\Routers\Route;
use Nette\Application\Routers\RouteList;
use Nette\DI\CompilerExtension;
use Nette\DI\Statement;
use Nette\PhpGenerator\ClassType;

class PackageExtension extends CompilerExtension
{
	protected $config;
	
	protected $defaults = [];
	
	public function loadConfiguration()
	{
		$config = $this->config = $this->getConfig($this->defaults);
		
		if (empty($this->config['extends'])) {
			throw new PackageException("Set 'extends' parameter in PackageExtension configuration in neon.");
		}


		$builder = $this->getContainerBuilder();
		
		$presenterFactory = $builder->getDefinition($builder->getByType(IPresenterFactory::class));
		$presenterFactory
			->addSetup('setMapping', [
				['AdminPackage' => '\CPTeam\Packages\*Package\Admin\Presenters\*Presenter'],
			]);
		
		$packageRouteList = new RouteList("AdminPackage");
		
		$packageServices = [];
		
		/** @var Statement $class */
		foreach ($config['packages'] as $class) {
			
			// konfigurace z applikace, z konstruktoru package
			$packageConfig = !empty($class->arguments[0]) ? $class->arguments[0] : [];
			
			// rozparsovani jmena
			$n = explode("\\", $class->getEntity());
			$name = substr(end($n), 0, -7);
			$nameLower = strtolower($name);
			
			$x = $builder->addDefinition($this->prefix($nameLower))
				->setClass($class->getEntity());
			
			$packageClassName = $x->getClass();
			/** @var Package $package */
			$package = new $packageClassName();
			
			$packageServices[] = $x;
			
			$installator = $x->getClass() . "Installator";
			
			/** @var PackageInstallator $packageInstallator */
			$packageInstallator = new $installator($builder, $nameLower);
			$packageInstallator->install(
				$package->getConfig($packageConfig)
			);
			
			//add package route
			$packageRouteList[] = $routeList = new RouteList($name);
			$routeList[] = new Route("admin/package/{$nameLower}[/<presenter>][/<action>][/<id>]", "Home:default");
		}
		
		// add packages route
		$packageRouteList[] = new Route("admin/packages/<presenter>[/<action>][/<id>]", "Dashboard:default");
		
		$ps = $builder->addDefinition($this->name)
			->setClass(PackageService::class);
		
		foreach ($packageServices as $packageService) {
			$ps->addSetup("addPackage", [$packageService]);
		}
		
		$routerFactory = $builder->getDefinition($builder->getByType(RouterFactory::class));
		$routerFactory->addSetup("addRouteList", [$packageRouteList]);
		
		$this->createBase($builder);
	}
	
	/**
	 * @param $builder
	 */
	private function createBase($builder)
	{
		//Create BasePresenter
		$trait = ClassType::from(TBasePresenter::class);
		$traitReflection = new \ReflectionClass(TBasePresenter::class);
		
		foreach ($trait->getMethods() as $classMethod) {
			$method = $traitReflection->getMethod($classMethod->getName());
			
			if ($method) {
				$lines = file($traitReflection->getFileName(), FILE_IGNORE_NEW_LINES);
				$lines = array_slice($lines, $method->getStartLine() - 1, ($method->getEndLine() - $method->getStartLine() + 1), true);
				$lines = implode("\n", $lines);
				$firstBracketPos = strpos($lines, '{');
				$lastBracketPost = strrpos($lines, '}');
				$body = substr($lines, $firstBracketPos + 1, $lastBracketPost - $firstBracketPos - 1);
				
				$body = str_ireplace("%appDir%", $builder->parameters['appDir'], $body);
				$body = str_ireplace("%wwwDir%", $builder->parameters['wwwDir'], $body);
				$body = str_ireplace("%tempDir%", $builder->parameters['tempDir'], $body);
				
				$classMethod->setBody(trim(rtrim($body), "\n\r"));
			}
		}
		
		$basePresenter = new ClassType("BasePresenter", $trait->getNamespace());
		$basePresenter->setExtends($this->config['extends']);
		$basePresenter->setMethods($trait->getMethods());
		
		if (file_exists($builder->parameters['tempDir'] . "/files/") === false) {
			mkdir($builder->parameters['tempDir'] . "/files/");
		}
		
		file_put_contents($builder->parameters['tempDir'] . "/files/{$basePresenter->getName()}.php", "<?php\n\n{$basePresenter->getNamespace()} $basePresenter");
	}
	
}
