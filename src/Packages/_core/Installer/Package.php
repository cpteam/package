<?php
/**
 * Created by PhpStorm.
 * User: lukys
 * Date: 2.10.16
 * Time: 23:25
 */

namespace CPTeam\Packages;

use CPTeam\LogicException;

/**
 * Class TestPackage
 *
 * @package CPTeam\Packages\TestPackage
 *
 * @property $name;
 * @property $fullName;
 */
abstract class Package
{
	/**
	 * @var string
	 */
	public $name;
	public $fullName;
	
	public $presenter = "Home";
	public $action = "default";
	
	protected $defaults = [];
	protected $config = null;
	
	/**
	 * @var \CPTeam\Packages\PackageService
	 */
	private $service;
	
	public function __construct()
	{
		$n = explode("\\", get_class($this));
		
		$this->fullName = substr(end($n), 0, -7);
		$this->name = strtolower($this->fullName);
	}
	
	/**
	 * @param \CPTeam\Packages\PackageService $service
	 */
	public function setPackageService(PackageService $service)
	{
		$this->service = $service;
	}
	
	/**
	 * @return \CPTeam\Packages\PackageService
	 */
	public function getService()
	{
		return $this->service;
	}
	
	public function getConfig(array $config = null)
	{
		if ($this->config === null) {
			$this->config = $this->defaults;
		}
		
		if ($config !== null) {
			if (is_array($this->config)) {
				$this->config = array_replace_recursive($this->config, $config);
			} else {
				$this->config = $config;
			}
		}
		
		return $this->config;
	}
}

class PackageException extends LogicException
{
	
}
