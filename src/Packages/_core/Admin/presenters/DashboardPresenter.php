<?php

namespace CPTeam\Packages\Admin\Presenters;

class DashboardPresenter extends BasePresenter
{
	
	public function startup()
	{
		parent::startup();
	}
	
	public function actionDefault()
	{
		$this->template->service = $this->getService('package');
	}
}
