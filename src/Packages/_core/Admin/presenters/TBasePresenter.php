<?php

namespace CPTeam\Packages\Admin\Presenters;

/**
 * Class BasePresenter
 *
 * @package CPTeam\Packages\Presenters
 *
 */
trait TBasePresenter
{
	
	public function beforeRender()
	{
		parent::beforeRender();
		
		$this->setLayout(
			$this->getPackageLayout()
		);
	}
	
	/**
	 * @return string
	 */
	private function getPackageLayout()
	{
		
		$routes = [
			"%appDir%/AdminModule/templates/@package.latte",
			"%appDir%/AdminModule/presenters/templates/@package.latte",
		];
		
		foreach ($routes as $route) {
			if (file_exists($route)) {
				return $route;
			}
		}
		
		\CPTeam\Tracy\Helpers\BlueScreen::addExceptionPreview(
			\CPTeam\Packages\PackageException::class,
			"Routes",
			__FILE__,
			29
		);
		
		throw new \CPTeam\Packages\PackageException("Package layout not found at these routes. ");
	}
}

