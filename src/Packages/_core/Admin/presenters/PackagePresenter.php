<?php
/**
 * Created by PhpStorm.
 * User: lukys
 * Date: 2.10.16
 * Time: 23:33
 */

namespace CPTeam\Packages\Admin\Presenters;

/**
 * Class PackagePresenter
 *
 * @package CPTeam\Bundles\Installer
 *
 */
abstract class PackagePresenter extends BasePresenter
{
	const PACKAGE_MODULE_NAME = "AdminPackage";
	const PACKAGE_SERVICE_NAME = "package";
	
	protected $package;
	
	private $packages;
	
	public function startup()
	{
		parent::startup();
		
		$name = explode(":", $this->request->getPresenterName());
		
		if ($name[0] !== self::PACKAGE_MODULE_NAME) {
		}
		
		$this->package = $this->context->getService('package.' . lcfirst($name[1]));
	}
	
	public function beforeRender()
	{
		parent::beforeRender();
		$this->template->package = $this->package;
	}
}
