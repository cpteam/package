<?php
/**
 * Created by PhpStorm.
 * User: lukys
 * Date: 2.10.16
 * Time: 23:25
 */

namespace CPTeam\Packages\TestPackage;

use CPTeam\Packages\Package;

/**
 * Class TestPackage
 *
 * @package CPTeam\Packages\TestPackage
 *
 * @property $name;
 */
class TestPackage extends Package
{
	public $description = "Description Test Packages";
}
