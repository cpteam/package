<?php
/**
 * Created by PhpStorm.
 * User: lukys
 * Date: 2.10.16
 * Time: 23:25
 */

namespace CPTeam\Packages\TestPackage;

use CPTeam\Packages\TestPackage\Components\TestControl\TestControl;

/**
 * Class TestPackage
 *
 * @package CPTeam\Packages\TestPackage
 *
 * @property $name;
 */
class TestPackageInstallator extends PackageInstallator
{
	
	/**
	 * @param array $config
	 */
	public function install(array $config)
	{
		$this->builder->addDefinition($this->prefix("testControl"))->setClass(TestControl::class);
		
		$this->builder->addDefinition($this->prefix("testControl58"))->setClass(TestControl::class);
	}
	
}
