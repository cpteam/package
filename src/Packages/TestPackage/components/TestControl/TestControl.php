<?php

namespace CPTeam\Packages\TestPackage\Components\TestControl;

use Nette\Application\UI\Control;

class TestControl extends Control
{
	
	public function render()
	{
		$this->template->setFile(__DIR__ . "/template.latte");
		$this->template->render();
	}
}
